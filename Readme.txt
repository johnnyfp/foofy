================================================================================
= Foofy and Foofy Mini - Codenamed Ant                                         =
= Arduino Setup guide                                                          =
================================================================================

This repository is for the files needed to compile and run
code on the Foofy and Foofy Mini.

Visit http://wiki.sharedcircuits.com/index.php/Foofy_IDE_Setup for setup of the 
Arduino tool chain.

Compiled against Current Teensyduino version 1.20