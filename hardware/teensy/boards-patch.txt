teensy31foofy.name=Teensy 3.1 Foofy
teensy31foofy.upload.protocol=halfkay
teensy31foofy.upload.maximum_size=262144
teensy31foofy.upload.maximum_ram_size=65536
teensy31foofy.upload.speed=12000000
teensy31foofy.upload.disable_flushing=true
teensy31foofy.bootloader.path=undefined
teensy31foofy.serial.restart_cmd=true
teensy31foofy.serial.safe_baud_rates_only=true
teensy31foofy.upload.avrdude_wrapper=teensy_reboot
teensy31foofy.build.mcu=mk20dx256
teensy31foofy.build.cpu=cortex-m4
teensy31foofy.build.option1=-mthumb
teensy31foofy.build.option2=-nostdlib
teensy31foofy.build.option3=-D__MK20DX256__
teensy31foofy.build.option4=-DTEENSYDUINO=120
teensy31foofy.build.option5=-Wno-write-strings
teensy31foofy.build.option6=-Wno-return-type
teensy31foofy.build.option7=-Wno-sign-compare
teensy31foofy.build.option8=-DF_CPU=96000000
teensy31foofy.build.option9=-DUSB_SERIAL
teensy31foofy.build.option10=-DLAYOUT_US_ENGLISH
teensy31foofy.build.cppoption1=-fno-rtti
teensy31foofy.build.linkoption1=-mthumb
teensy31foofy.build.additionalobject1=-larm_cortexM4l_math
teensy31foofy.build.linkscript=mk20dx256.ld
teensy31foofy.build.architecture=arm-none-eabi
teensy31foofy.build.command.gcc=arm-none-eabi-gcc
teensy31foofy.build.command.g++=arm-none-eabi-g++
teensy31foofy.build.command.ar=arm-none-eabi-ar
teensy31foofy.build.command.objcopy=arm-none-eabi-objcopy
teensy31foofy.build.command.objdump=arm-none-eabi-objdump
teensy31foofy.build.command.size=arm-none-eabi-size
teensy31foofy.build.core=teensy3foofy
teensy31foofy.build.elide_constructors=true
teensy31foofy.build.gnu0x=true
teensy31foofy.build.dependency=true
teensy31foofy.build.time_t=true
teensy31foofy.build.post_compile_script=teensy_post_compile
teensy31foofy.menu.usb.serial.name=Serial
teensy31foofy.menu.usb.serial.build.define0=-DUSB_SERIAL
teensy31foofy.menu.usb.hid.name=Keyboard + Mouse + Joystick
teensy31foofy.menu.usb.hid.build.define0=-DUSB_HID
teensy31foofy.menu.usb.hid.fake_serial=teensy_gateway
teensy31foofy.menu.usb.serialhid.name=Serial + Keyboard + Mouse + Joystick
teensy31foofy.menu.usb.serialhid.build.define0=-DUSB_SERIAL_HID
#teensy31foofy.menu.usb.int_disk.name=Disk(Internal) + Keyboard
#teensy31foofy.menu.usb.int_disk.build.define0=-DUSB_DISK
#teensy31foofy.menu.usb.int_disk.fake_serial=teensy_gateway
#teensy31foofy.menu.usb.int_disk.build.elfpatch=mktinyfat
#teensy31foofy.menu.usb.sd_disk.name=Disk(SD Card) + Keyboard
#teensy31foofy.menu.usb.sd_disk.build.define0=-DUSB_DISK_SDFLASH
#teensy31foofy.menu.usb.sd_disk.fake_serial=teensy_gateway
teensy31foofy.menu.usb.midi.name=MIDI
teensy31foofy.menu.usb.midi.build.define0=-DUSB_MIDI
teensy31foofy.menu.usb.midi.fake_serial=teensy_gateway
teensy31foofy.menu.usb.rawhid.name=Raw HID
teensy31foofy.menu.usb.rawhid.build.define0=-DUSB_RAWHID
teensy31foofy.menu.usb.rawhid.fake_serial=teensy_gateway
teensy31foofy.menu.usb.flightsim.name=Flight Sim Controls
teensy31foofy.menu.usb.flightsim.build.define0=-DUSB_FLIGHTSIM
teensy31foofy.menu.usb.flightsim.fake_serial=teensy_gateway
teensy31foofy.menu.speed.168.name=168 MHz (overclock)
teensy31foofy.menu.speed.144.name=144 MHz (overclock)
teensy31foofy.menu.speed.120.name=120 MHz (overclock)
teensy31foofy.menu.speed.96.name=96 MHz (overclock)
teensy31foofy.menu.speed.72.name=72 MHz
teensy31foofy.menu.speed.48.name=48 MHz
teensy31foofy.menu.speed.24.name=24 MHz
teensy31foofy.menu.speed.16nousb.name=16 MHz (No USB)
teensy31foofy.menu.speed.8nousb.name=8 MHz (No USB)
teensy31foofy.menu.speed.4nousb.name=4 MHz (No USB)
teensy31foofy.menu.speed.2nousb.name=2 MHz (No USB)
teensy31foofy.menu.speed.168.build.f_cpu=168000000
teensy31foofy.menu.speed.144.build.f_cpu=144000000
teensy31foofy.menu.speed.120.build.f_cpu=120000000
teensy31foofy.menu.speed.96.build.f_cpu=96000000
teensy31foofy.menu.speed.72.build.f_cpu=72000000
teensy31foofy.menu.speed.48.build.f_cpu=48000000
teensy31foofy.menu.speed.24.build.f_cpu=24000000
teensy31foofy.menu.speed.16nousb.build.f_cpu=16000000
teensy31foofy.menu.speed.8nousb.build.f_cpu=8000000
teensy31foofy.menu.speed.4nousb.build.f_cpu=4000000
teensy31foofy.menu.speed.2nousb.build.f_cpu=2000000
teensy31foofy.menu.keys.en-us.name=US English
teensy31foofy.menu.keys.en-us.build.define1=-DLAYOUT_US_ENGLISH
teensy31foofy.menu.keys.fr-ca.name=Canadian French
teensy31foofy.menu.keys.fr-ca.build.define1=-DLAYOUT_CANADIAN_FRENCH
teensy31foofy.menu.keys.xx-ca.name=Canadian Multilingual
teensy31foofy.menu.keys.xx-ca.build.define1=-DLAYOUT_CANADIAN_MULTILINGUAL
teensy31.menu.keys.cz-cz.name=Czech
teensy31.menu.keys.cz-cz.build.define1=-DLAYOUT_CZECH
teensy31foofy.menu.keys.da-da.name=Danish
teensy31foofy.menu.keys.da-da.build.define1=-DLAYOUT_DANISH
teensy31foofy.menu.keys.fi-fi.name=Finnish
teensy31foofy.menu.keys.fi-fi.build.define1=-DLAYOUT_FINNISH
teensy31foofy.menu.keys.fr-fr.name=French
teensy31foofy.menu.keys.fr-fr.build.define1=-DLAYOUT_FRENCH
teensy31foofy.menu.keys.fr-be.name=French Belgian
teensy31foofy.menu.keys.fr-be.build.define1=-DLAYOUT_FRENCH_BELGIAN
teensy31foofy.menu.keys.fr-ch.name=French Swiss
teensy31foofy.menu.keys.fr-ch.build.define1=-DLAYOUT_FRENCH_SWISS
teensy31foofy.menu.keys.de-de.name=German
teensy31foofy.menu.keys.de-de.build.define1=-DLAYOUT_GERMAN
teensy31foofy.menu.keys.de-dm.name=German (Mac)
teensy31foofy.menu.keys.de-dm.build.define1=-DLAYOUT_GERMAN_MAC
teensy31foofy.menu.keys.de-ch.name=German Swiss
teensy31foofy.menu.keys.de-ch.build.define1=-DLAYOUT_GERMAN_SWISS
teensy31foofy.menu.keys.is-is.name=Icelandic
teensy31foofy.menu.keys.is-is.build.define1=-DLAYOUT_ICELANDIC
teensy31foofy.menu.keys.en-ie.name=Irish
teensy31foofy.menu.keys.en-ie.build.define1=-DLAYOUT_IRISH
teensy31foofy.menu.keys.it-it.name=Italian
teensy31foofy.menu.keys.it-it.build.define1=-DLAYOUT_ITALIAN
teensy31foofy.menu.keys.no-no.name=Norwegian
teensy31foofy.menu.keys.no-no.build.define1=-DLAYOUT_NORWEGIAN
teensy31foofy.menu.keys.pt-pt.name=Portuguese
teensy31foofy.menu.keys.pt-pt.build.define1=-DLAYOUT_PORTUGUESE
teensy31foofy.menu.keys.pt-br.name=Portuguese Brazilian
teensy31foofy.menu.keys.pt-br.build.define1=-DLAYOUT_PORTUGUESE_BRAZILIAN
teensy31foofy.menu.keys.es-es.name=Spanish
teensy31foofy.menu.keys.es-es.build.define1=-DLAYOUT_SPANISH
teensy31foofy.menu.keys.es-mx.name=Spanish Latin America
teensy31foofy.menu.keys.es-mx.build.define1=-DLAYOUT_SPANISH_LATIN_AMERICA
teensy31foofy.menu.keys.sv-se.name=Swedish
teensy31foofy.menu.keys.sv-se.build.define1=-DLAYOUT_SWEDISH
teensy31foofy.menu.keys.tr-tr.name=Turkish (partial)
teensy31foofy.menu.keys.tr-tr.build.define1=-DLAYOUT_TURKISH
teensy31foofy.menu.keys.en-gb.name=United Kingdom
teensy31foofy.menu.keys.en-gb.build.define1=-DLAYOUT_UNITED_KINGDOM
teensy31foofy.menu.keys.usint.name=US International
teensy31foofy.menu.keys.usint.build.define1=-DLAYOUT_US_INTERNATIONAL